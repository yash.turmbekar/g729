﻿using System;
using System.IO;
using GroovyCodecs.G729;
using GroovyCodecs.Mp3;
using GroovyCodecs.WavFile;

namespace GroovyCodecs.Coder
{
	class Program
	{
		private static IMp3Encoder _lameEnc;
		private static IG729Encoder _G729Encoder;
		private static IG729Decoder _G729Decoder;

		static void Main(string[] args)
        {
            //Mp3Convertor();
            G729Convertor();
            //G729DecoderConvertor();
        }

        private static void G729Convertor()
        {
            var files = Directory.GetFiles("./testfiles/", "*.wav", SearchOption.AllDirectories);

            foreach (var file in files)
            {
                _G729Encoder = new G729Encoder();

                var audioFile = new WavReader();
                audioFile.OpenFile(file);

                //var srcFormat = audioFile.GetFormat();

                //_G729Encoder.SetFormat(srcFormat, srcFormat);

                var inBuffer = audioFile.readWav();

                var outBuffer = new byte[inBuffer.Length];

                var timer = new System.Diagnostics.Stopwatch();
                timer.Start();
                outBuffer = _G729Encoder.Process(inBuffer);
                timer.Stop();

                _G729Encoder.Flush();


                //var outFile = File.Create(file + ".g729");

                File.WriteAllBytes(file + ".g729", outBuffer);
                //outFile.Write(outBuffer, 0, outBuffer.Length);
                //outFile.Close();

                Console.WriteLine($"Converted {file} to g729 in {timer.ElapsedMilliseconds / 1000}s");
            }
        }

        private static void G729DecoderConvertor()
        {
            var files = Directory.GetFiles("./testfiles/", "*.g729", SearchOption.AllDirectories);

            foreach (var file in files)
            {
                _G729Decoder = new G729Decoder();

                //var audioFile = new WavReader();
                //audioFile.OpenFile(file);
                //var srcFormat = audioFile.GetFormat();

                //_G729Encoder.SetFormat(srcFormat, srcFormat);

                //var inBuffer = audioFile.readWav();

                var inBuffer = File.ReadAllBytes(file);

                //var outBuffer = new byte[inBuffer.Length];

                var timer = new System.Diagnostics.Stopwatch();
                timer.Start();
                var outBytes = _G729Decoder.Process(inBuffer);
                timer.Stop();

                var outFile = File.Create(file + ".wav");
                outFile.Write(outBytes, 0, outBytes.Length);
                outFile.Close();

                Console.WriteLine($"Converted {file} to wav in {timer.ElapsedMilliseconds / 1000}s");
            }
        }
   
        private static void Mp3Convertor()
        {
            var files = Directory.GetFiles("./testfiles/", "*.wav", SearchOption.AllDirectories);

            foreach (var file in files)
            {
                _lameEnc = new Mp3Encoder();

                var audioFile = new WavReader();
                audioFile.OpenFile(file);

                var srcFormat = audioFile.GetFormat();

                _lameEnc.SetFormat(srcFormat, srcFormat);

                var inBuffer = audioFile.readWav();

                var outBuffer = new byte[inBuffer.Length];

                var timer = new System.Diagnostics.Stopwatch();
                timer.Start();
                var len = _lameEnc.EncodeBuffer(inBuffer, 0, inBuffer.Length, outBuffer);
                timer.Stop();

                _lameEnc.Close();

                // _lameDec = new LameDecoder();

                var outFile = File.Create(file + ".mp3");
                outFile.Write(outBuffer, 0, len);
                outFile.Close();

                var outFile1 = File.Create(file + ".g729");
                outFile1.Write(outBuffer, 0, len);
                outFile1.Close();

                Console.WriteLine($"Converted {file} to MP3 in {timer.ElapsedMilliseconds / 1000}s");
            }
        }
   
    }

}
