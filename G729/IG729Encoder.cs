﻿/*
 * Copyright @ 2015 Atlassian Pty Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * G.729 is now patent free. See:
 * <a href="http://www.sipro.com">SIPRO Lab Telecom</a>.
 */
/* ITU-T G.729 Software Package Release 2 (November 2006) */
/*
 * ITU-T G.729 Annex C - Reference C code for floating point
 * implementation of G.729
 * Version 1.01 of 15.September.98
*/
/*
----------------------------------------------------------------------
            COPYRIGHT NOTICE
----------------------------------------------------------------------
ITU-T G.729 Annex C ANSI C source code
Copyright (C) 1998, AT&T, France Telecom, NTT, University of
Sherbrooke.  All rights reserved.

----------------------------------------------------------------------
*/
/**
 * Main program of the ITU-T G.729   8 kbit/s encoder.
 *
 * @author Lubomir Marinov (translation of ITU-T C source code to Java)
 */
/*
 * Converted to C# for GroovyG7xx, see https://github.com/jongoochgithub/GroovyCodecs
 */

namespace GroovyCodecs.G729
{
    public interface IG729Encoder
    {
        byte[] Flush();
        byte[] Process(byte[] speech);
    }
}